<x-layout>
    <x-navbar></x-navbar>
    <div class="container mt-5">
        <div class="row">
          <div class="col-12  col-md-6 offset-md-3">
            <h2>Dettaglio</h2>
            <div class="card box-color">
              @if ($article->img)
              <img class="card-img-top img-fluid" src="{{Storage::url($article->img)}}" alt="Card image cap">   
               @else
              <img src="/img/default_img.png" class="img-fluid" alt=""> 
              @endif
              <div class="card-body">
                <h5 class="card-title">{{$article->title}}</h5>
                <p class="card-text">{{$article->description}}</p>
                <a href="{{route('homepage')}}" class="btn btn-primary">Torna alla home</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <x-footer></x-footer>
</x-layout>