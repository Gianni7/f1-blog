<x-layout>
    <header class="masthead1">
        <div class="container h-100">
          <div class="row h-100 align-items-center">
            <div class="col-12 text-center">
            </div>
          </div>
        </div>
      </header>
      <div class="container mt-5 box-color">
          <div class="row">
              <div class="col-12">
                  <h2 class="mt-4">Partecipa alla discussione, inserisci il tuo articolo</h2>
              </div>
          </div>
          <div class="col-12 col-md-6 offset-md-3">
            <div class="row text-white mt-3">
              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              <form method="POST" action="{{route('article.store')}}" enctype="multipart/form-data">
                @csrf
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Titolo</label>
                  <input type="text" name="title" class="form-control" aria-describedby="emailHelp" value="{{old('title')}}">
                </div>
                <div class="mb-3">
                  <label for="image" class="form-label">Immagine</label>
                  <input type="file" name="img" class="form-control" id="image">
                </div>
                <div class="mb-3">
                  <div>
                  <label for="exampleInputEmail1" class="form-label">Articolo</label>
                  </div>
                  <textarea name="description" id="" cols="50" rows="5">{{old('description')}}</textarea>
                </div>
                <button type="submit" class="btn btn-primary mb-3">Invia</button>
              </form>
            </div>
          </div>
      </div>
      <x-footer></x-footer>
</x-layout>