<x-layout>
  <x-navbar></x-navbar>
    <header class="masthead">
        <div class="container h-100">
          <div class="row h-100 align-items-center">
            <div class="col-12 text-center">
            </div>
          </div>
        </div>
      </header>
      <div class="container mt-5">
        <div class="row">
          @foreach ($articles as $article)
          <div class="col-12 col-md-3 col-xl-4">
            <div class="card box-color">
              @if ($article->img)
              <img class="card-img-top img-fluid" src="{{Storage::url($article->img)}}" alt="Card image cap">   
               @else
              <img src="/img/default_img.png" class="img-fluid" alt=""> 
              @endif
              <div class="card-body">
                <h5 class="card-title">{{$article->title}}</h5>
                <p class="card-text">{{$article->description}}</p>
                <a href="{{route('article.detail', compact('article'))}}" class="btn btn-primary">Dettaglio</a>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
      <x-footer></x-footer>
</x-layout>