<x-layout>
    <x-navbar></x-navbar>
    <header class="masthead2">
        <div class="container h-100">
          <div class="row h-100 align-items-center">
            <div class="col-12 text-center">
            </div>
          </div>
        </div>
      </header>
      <div class="container mt-5 box-color">
        <div class="row">
            <div class="col-12">
                <h2 class="mt-4">Compila il form e registrati</h2>
            </div>
        </div>
        <div class="col-12 col-md-6 offset-md-3">
          <div class="row text-white mt-3">
            <form method="POST" action="{{route('register')}}">
              @csrf
              <div class="mb-3">
                <label for="name1" class="form-label">Nome utente</label>
                <input type="text" name="name" class="form-control" id="name1">
              </div>
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Indirizzo email</label>
                  <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label">Password</label>
                  <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                </div>
                <div class="mb-3">
                    <label for="password_confirmation" class="form-label">Conferma password</label>
                    <input type="password" name="password_confirmation" class="form-control" id="password_confirmation">
                  </div>
                <button type="submit" class="btn btn-primary mb-3">Invia</button>
              </form>
          </div>
        </div>
    </div>
    <x-footer></x-footer>
</x-layout>





