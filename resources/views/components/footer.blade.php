<footer class="continer-fluid footer-color">
    <div class="container mt-5">
        <div class="row">
            <div class="col-3"></div>
            <div class="col-6 text-center mt-4">
                <h3>Seguici su:</h3>
            </div>
            <div class="col-3"></div>
        </div>
            <div class="row text-center mt-4 mb-4">
                <div class="col mb-5">
                    <h4><i class="fab fa-facebook-f"></i></h4>
                </div>
                <div class="col">
                    <h4><i class="fab fa-instagram"></i></h4>
                </div>
                <div class="col">
                <h4><i class="fab fa-twitter"></i></h4>
                </div>
            </div>
    </div>
    <div class="col-12 my-2 py-2 text-center">
       <p>© 2021 | P.Iva 123456789 | Cookie Policy | Privacy Policy</p>
      </div>
</footer>

