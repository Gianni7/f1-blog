<div class="container">
<nav class="navbar navbar-expand-lg navbar-light bg-custon">
    <div class="container-fluid">
      <a class="navbar-brand my-3 fsize" href="{{route('homepage')}}"><span id="colorf1">F1</span>Blog <i class="fas fa-flag-checkered"></i></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fas fa-ellipsis-v"></i>
        {{-- <span class="navbar-toggler-icon toggler-red"></span> --}}
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav ms-auto my-3">
          <li class="nav-item">
           @guest 
            <a class="nav-link active me-3" aria-current="page" href="{{route('homepage')}}"><i class="fas fa-home"></i> Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link me-3" href="{{route('red.index')}}"><i class="fas fa-car"></i> Ferrari</a>
          </li>
          <li class="nav-item">
            <a class="nav-link me-3" href="{{route('login')}}"><i class="fas fa-sign-in-alt"></i> Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('register')}}"><i class="fas fa-pen"></i> Registrati</a>
          </li>
          @else
          <li class="nav-item">
            <a class="nav-link me-3" href="{{route('article.create')}}"><i class="far fa-newspaper"></i> Inserisci Articolo</a>
          </li>
          <li class="nav-item">
            <a class="nav-link me-3" href="{{route('red.create')}}"><i class="far fa-newspaper"></i> Sezione Ferrari</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              <i class="fas fa-user"></i>  Ciao, {{Auth::user()->name}}
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <li><a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault();
                document.getElementById('form-logout').submit();">Logout</a></li>
                <form method="POST" action="{{route('logout')}}" style="display: none" id="form-logout">
                  @csrf
                </form>
            </ul>
          </li>
          @endguest
        </ul>
      </div>
    </div>
  </nav>
</div>

