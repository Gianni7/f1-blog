<?php


use App\Http\Controllers\ArticleController;
use App\Http\Controllers\RedController;
use App\Http\Controllers\PublicController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ArticleController::class, 'homepage'])->name('homepage');
Route::get('/article/create', [ArticleController::class, 'create'])->name('article.create');
Route::post('/article/store', [ArticleController::class, 'store'])->name('article.store');
Route::get('/article/detail/{article}', [ArticleController::class,'detail'])->name('article.detail');
Route::get('/red/create', [RedController::class, 'create'])->name('red.create');
Route::post('/red/store', [RedController::class, 'store'])->name('red.store');
Route::get('/red/index', [RedController::class, 'index'])->name('red.index');
Route::get('/red/show/{red}', [RedController::class, 'show'])->name('red.show');
