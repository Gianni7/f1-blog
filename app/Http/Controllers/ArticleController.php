<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequest;

class ArticleController extends Controller
{   
    public function homepage () {

        $articles = Article::all();

        return view('welcome', compact('articles'));
    }


    public function create() {
        return view('article.form');
    }

    public function store(ArticleRequest $req) {
       
        $article = Article::create([
            'title' => $req->input('title'),
            'description' => $req->input('description'),
            'img'=> $req->file('img')->store('public/img'),

        ]);

        return redirect(route('homepage'));
    }


    public function detail(Article $article) {
        return view ('article.detail', compact('article'));
    }


}
